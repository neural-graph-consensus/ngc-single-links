"""aggregation function for NGC"""
from typing import Callable, List, Dict
from omegaconf import DictConfig, OmegaConf

from torch import nn
import torch as tr

class Conv3D_V3(nn.Module):
    """
    540x1080 -> conv3ds -> B,V,W,H,D
    out = B,V,W,H,D x B,V,W,H,D -> B,V,W,H,D (1 weight for each pixel])
    """

    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2
        self.n_votes = n_votes

        shapes = [n_votes, *hidden_shapes]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.conv3d_out = nn.Conv3d(in_channels=shapes[-1], out_channels=n_votes, kernel_size=3, padding=1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = self.conv3ds(x)
        y_mask = self.conv3d_out(y)
        y_out = tr.einsum("bvwhd, bvwhd -> bwhd", x, y_mask)
        return y_out

# TODO: great name
# TODO: not generic, only conv3d_v3
class NNOnePerNode(nn.Module):
    """One NN per each output node stored in a module dict"""
    def __init__(self, out_node_voters: Dict[str, int], base_net: str, hidden_shapes: List[int]):
        super().__init__()
        assert base_net == "conv3d_v3", base_net
        self.heads = nn.ModuleDict()
        for out_node, n_voters in out_node_voters.items():
            self.heads[out_node] = Conv3D_V3(n_voters, hidden_shapes)

    def forward(self, votes: tr.Tensor, sources: List[str]) -> tr.Tensor:
        sources = [x.split("-> ")[-1] for x in sources]
        assert len(set(sources)) == 1, sources
        return self.heads[sources[0]](votes)

class NNOnePerGraph(nn.Module):
    """One NN for the entire graph aggregation. Each out_nodes must have identical number of voters then."""
    def __init__(self, n_votes: int, base_net: str, hidden_shapes: List[int]):
        super().__init__()
        assert base_net == "conv3d_v3", base_net
        self.base_net = Conv3D_V3(n_votes, hidden_shapes)

    def forward(self, votes: tr.Tensor, sources: List[str]) -> tr.Tensor:
        return self.base_net(votes)

def aggregation_fn_from_cfg(cfg: DictConfig) -> Callable:
    """given a cfg with cfg.graph.aggregation node return the aggregation function"""
    if cfg.graph.aggregation.type == "mean":
        return lambda votes, sources: votes.mean(dim=1)
    if cfg.graph.aggregation.type == "median":
        return lambda votes, sources: votes.median(dim=1)[0]
    if cfg.graph.aggregation.type == "nn_one_per_node":
        # TODO: probbly not generic. Works for NGC-V1.
        in_nodes = cfg.graph.nodes.inputNodes
        out_nodes = set(cfg.graph.nodes.names).difference(in_nodes)
        out_node_voters = {out_node: 0 for out_node in out_nodes}
        for edges_of_a_type in cfg.graph.edges.values():
            for edge in edges_of_a_type:
                out_node_voters[edge[-1]] += 1
        return NNOnePerNode(out_node_voters, **OmegaConf.to_container(cfg.graph.aggregation.parameters))
    if cfg.graph.aggregation.type == "nn_one_per_graph":
        # TODO: probbly not generic. Works for NGC-V1.
        in_nodes = cfg.graph.nodes.inputNodes
        out_nodes = set(cfg.graph.nodes.names).difference(in_nodes)
        out_node_voters = {out_node: 0 for out_node in out_nodes}
        for edges_of_a_type in cfg.graph.edges.values():
            for edge in edges_of_a_type:
                out_node_voters[edge[-1]] += 1
        n_voters = list(set(out_node_voters.values()))
        assert len(n_voters) == 1, f"nn_one_per_graph only works if all the out nodes have same n_voters: {n_voters}"
        return NNOnePerGraph(n_voters[0], **OmegaConf.to_container(cfg.graph.aggregation.parameters))

    raise KeyError(f"Unknown aggregation: '{cfg.graph.aggregation}'")
