"""Get model type"""
from typing import Callable
from torch import nn
from .safeuav import SafeUAV

def get_model_type(in_dim, out_dim, str_model_type: str, model_args: dict = {}) -> Callable:
    # TODO: This is not enough for Normals needing unit vectors
    if str_model_type == "linear":
        return nn.Linear(in_dim, out_dim, **model_args)
    if str_model_type == "safeuav":
        return SafeUAV(in_dim, out_dim, num_filters=model_args.get("num_filters", 16))
    raise ValueError(f"unknown: {str_model_type}")
