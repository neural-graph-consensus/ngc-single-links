from typing import Callable, List
from functools import partial
from ngclib import logger
import pandas as pd
import ast
import torch as tr

def _agg_lr(votes: "tr.Tensor[bvwhd]", sources: List[str], weights: pd.DataFrame) -> "tr.Tensor[bwhd]":
    """Vote function based on pre-set linear regression weights, for each node"""
    node = sources[0].split(" -> ")[-1]
    df = weights.loc[node]
    weights = df["weights"]
    edges = df["edges"]

    ixs = [edges.index(s) for s in sources]
    ordered_weights = tr.Tensor(weights)[ixs].to(votes.device)
    res = tr.einsum("bvwhd, v -> bwhd", votes, ordered_weights)
    return res

def vote_fn_lr(iteration: int) -> Callable:
    assert iteration <= 2, iteration

    if iteration == 1:
        df_path = "/scratch/nvme0n1/ngc/ngc-analyze-dump-dir/dump_dirs/nasa/dump_dir_it1_train/lr_weights_it1.csv"
        # lr_weights_it1.csv is applied for iteration 1 voting -> pseudos will be generated for iteration 2 training
    if iteration == 2:
        df_path = "/scratch/nvme0n1/ngc/ngc-analyze-dump-dir/notebooks/../dump_dirs/nasa/dump_dir_it2_lr_train/lr_weights_it2.csv"

    logger.info(f"Reading linear regression learned weights from '{df_path}'")
    df = pd.read_csv(df_path, index_col=0,
                     converters={"edges": ast.literal_eval, "weights": ast.literal_eval})

    return partial(_agg_lr, weights=df)
