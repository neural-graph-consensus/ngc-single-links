# NGC-Training general repository


## Training:

```
CUDA_VISIBLE_DEVICES=2 python train.py  \
	--train_set_path /path/to/train_set/ \
	--validation_set_path /path/to/validation_set/ \
	--config_path /path/to/config.yaml \
	[--semisupervised_set_path /path/to/semisupervised_set] \
	[--nodes_path /path/to/nodes/dir] \
	[--parallelize] \
	[--multi_train_count N]
```

Note: cannot use both `--parallelize` and `--multi_train_count` with CUDA. Ideally: use `--multi_train_count N` with
N = n_gpus and CUDA or use `--parallelize` and a single train for each edge. First will ensure each edge is trained
N times (1 per gpu), 2nd will ensure the edges are distributed to the N gpus evenly to train the graph faster. For
CPU, ensure that you have enough RAM, but you can do both levels of parallelism.

### Dataset format

Dataset must be in the standard format. For example, for `rgb -> depthSfm` single link, we need the rgb and
depthSfm npz files:


```
/path/to/train_set/
	rgb/
		0.npz, ..., N.npz
	depthSfm/
		0.npz, ..., N.npz
```

The config file must contain both `graph`, `train` and `data` primitives.
See [this example](cfgs/nasa/example_sl_only.yml).

### Semi-supervised iterative training

If `--semisupervised_set_path` is not set, only 1 iteration can be done (supervised).

### Nodes path

If `--nodes_path` is not provided, they must be loadable (same names/types as in the cfg) from `nodes/`. Two
repositories are implemented in ngclib so far, so you can use these.
For example: `--nodes_path /path/to/ngclib/nodes_repository/nasa`

## Evaluation

### Dump predictions

The `dump_predictions.py` script exports the **edge-level** predictions, alongside the aggregations. This means that
we can evaluate each edge individually (input->output) as well as output node-level predictions (after aggregation).

```
python dump_predictions.py \
	--dataset_path /path/to/test_set \
	--config_path /path/to/config.yaml \
	--nodes_path /path/to/nodes/dir \
	--weights_path /path/to/weights \
	-o /path/to/output_dump_dir \
	[--overwrite] \
	[--dump_png]
```

`--weights_path` must be the directory of `iterX/models` for async NGC and the `.ckpt` path for sync (GNN) NGC.

### Evaluate dumped predictions

```
python evaluate_dumped_predictions.py /path/to/output_dump_dir
```

This should yield in a `.csv` file in `/path/to/output_dump_dir/edge.csv` for each edge.

