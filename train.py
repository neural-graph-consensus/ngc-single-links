#!/usr/bin/env python3
"""
Training script for ngclib on various use-cases.
TODOs:
- nn/lr is not automated yet, we need to train them async after the edges are trained; load them: see vote_fn_lr/nn.py
"""

from functools import partial
from pathlib import Path
from typing import Dict
from argparse import ArgumentParser
from omegaconf import OmegaConf
import torch as tr
import numpy as np

from nwgraph import Node
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotCallback
from ngclib.logger import logger
from ngclib.readers import NGCNpzReader
from ngclib.ngcdir import NGCDir
from ngclib.trainer.async_trainer import NGCAsyncTrainer
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.models import NGC
from ngclib.semisupervised import make_iter1_data, pseudo_algo
from media_processing_lib.image import to_image, image_write
from media_processing_lib.collage_maker import collage_fn

from aggregation import aggregation_fn_from_cfg
from models import get_model_type
from vote_fn_nn import vote_fn_nn
from vote_fn_lr import vote_fn_lr

VOTE_FN = "MEAN"

def nodes_plot_fn(x: Dict[str, tr.Tensor], y: tr.Tensor, gt: Dict[str, tr.Tensor], out_dir: Path, model: LME):
    """Exports x, y and gt for each item in a batch, during training"""
    base_graph: NGC = model.base_model.base_graph
    in_node: Node = base_graph.edges[-1].input_node
    out_node: Node = base_graph.edges[-1].output_node
    to_img = lambda x: to_image(np.clip(x, 0, 1))

    y_in = y[in_node.name].cpu().numpy()
    y_out = y[out_node.name].cpu().numpy()
    gt = gt[out_node.name].cpu().numpy()
    # Remove the predictions where we don't have output predictions
    y_out[gt == 0] = 0

    for i in range(len(gt)):
        x_img = in_node.plot_fn(y_in[i]) if hasattr(in_node, "plot_fn") else to_img(y_in[i, ..., 0])
        y_img = out_node.plot_fn(y_out[i]) if hasattr(out_node, "plot_fn") else to_img(y_out[i, ..., 0])
        gt_img = out_node.plot_fn(gt[i]) if hasattr(out_node, "plot_fn") else to_img(gt[i])
        titles = [in_node.name, out_node.name, "GT"]
        img = collage_fn([x_img, y_img, gt_img], rows_cols=(1, 3), titles=titles)
        image_write(img, out_dir / f"{i}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the train & graph config path")
    parser.add_argument("--dataset_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--validation_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--semisupervised_set_path", type=lambda p: Path(p).absolute() if p is not None else None)
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    parser.add_argument("--parallelize", action="store_true")
    parser.add_argument("--multi_train_count", type=int, help="Number of independent train runs for each edge")
    args = parser.parse_args()
    if args.nodes_path is None:
        args.nodes_path = Path.cwd() / "nodes"
        logger.warning(f"--nodes_path not provided. Defaulting to '{args.nodes_path}'")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    graph_cfg = GraphCfg(OmegaConf.to_container(cfg.graph))
    if "experiment_name" not in cfg:
        cfg.experiment_name = args.config_path.stem
        logger.warning(f"Experiment name not provided. Defaulting to '{cfg.experiment_name}'")

    experiment_dir: Path = Path(__file__).parent / "experiments" / cfg.experiment_name
    Path(experiment_dir).mkdir(exist_ok=True, parents=True)
    logger.info(f"Experiment dir: '{experiment_dir}'")
    ngcdir = NGCDir(experiment_dir, graph_cfg)
    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, nodes_module_path=args.nodes_path).nodes

    validation_reader = NGCNpzReader(path=args.validation_set_path, nodes=nodes, out_nodes=graph_cfg.output_nodes)

    ngc_edge_fn = partial(get_model_type, str_model_type=cfg.model.type, model_args=cfg.model.parameters)
    vote_fn = aggregation_fn_from_cfg(cfg)
    graph = graph_cfg.build_model(nodes, ngc_edge_fn, vote_fn).to("cpu")
    graph.to_graphviz().render(ngcdir.path / "graph", format="png", cleanup=True)

    loader_params = OmegaConf.to_container(cfg.data.loader_params)
    train_cfg = OmegaConf.to_container(cfg.train)
    trainer_params = {**train_cfg["trainer_params"], "callbacks": [PlotCallback(nodes_plot_fn)]}
    strategy = "parallel" if args.parallelize else "sequential"

    # Iter 1
    logger.info(f"Iteration 1 trained: {ngcdir.is_iteration_trained(iteration=1)}")
    if not ngcdir.is_iteration_trained(iteration=1):

        # prepare data
        make_iter1_data(args.dataset_path, experiment_dir / "iter1/data", graph_cfg.node_names)
        reader_it1 = NGCNpzReader(path=experiment_dir / "iter1/data", nodes=nodes, out_nodes=graph_cfg.output_nodes)

        trainer = NGCAsyncTrainer(graph, train_cfg=train_cfg, train_reader=reader_it1,
                                  validation_reader=validation_reader, iter_dir=experiment_dir / "iter1/models",
                                  dataloader_params=loader_params, trainer_params=trainer_params,
                                  seed=cfg.seed, strategy=strategy, multi_train_count=args.multi_train_count)
        trainer()

    if args.semisupervised_set_path is None:
        logger.info("No semi supervised path provided. Stopping after 1 iteration")
        exit(0)

    exit(0)

    # Iter 2
    logger.info(f"Iteration 2 trained: {ngcdir.is_iteration_trained(iteration=2)}")
    if not ngcdir.is_iteration_trained(iteration=2):
        # Pseudolabels for It2
        graph.load_all_weights(args.experiment_name / "iter1/models")
        if VOTE_FN == "mean":
            graph.vote_fn = lambda votes, sources: votes.mean(dim=1)
        elif VOTE_FN == "NN":
            graph.vote_fn = vote_fn_nn(iteration=2)
        elif VOTE_FN == "LR":
            graph.vote_fn = vote_fn_lr(iteration=2)
        pseudo_algo(graph, args.semisupervised_set_path, args.experiment_name / "iter2/data", batch_size=5)
        make_iter1_data(args.dataset_path, args.experiment_name / "iter2/data", graph_cfg.node_names)

        # prepare data
        reader_it2 = NGCNpzReader(args.experiment_name / "iter2/data", nodes=nodes, out_nodes=graph_cfg.output_nodes)

        # train
        # TODO: separate ens edges from overall graph ensembles
        graph.vote_fn = vote_fn_median # median for ensemble edges.
        trainer = NGCAsyncTrainer(graph, train_cfg=train_cfg, train_reader=reader_it2,
                                  validation_reader=validation_reader, iter_dir=experiment_dir / "iter2/models",
                                  dataloader_params=loader_params, trainer_params=trainer_params,
                                  seed=cfg.seed, strategy=strategy)
        trainer()

    # Iter 3
    logger.info(f"Iteration 3 trained: {ngcdir.is_iteration_trained(iteration=3)}")
    if not ngcdir.is_iteration_trained(iteration=3):
        # Pseudolabels for It3
        graph.load_all_weights(args.experiment_name / "iter2/models")
        if VOTE_FN == "mean":
            graph.vote_fn = lambda votes, sources: votes.mean(dim=1)
        elif VOTE_FN == "NN":
            graph.vote_fn = vote_fn_nn(iteration=2)
        elif VOTE_FN == "LR":
            graph.vote_fn = vote_fn_lr(iteration=2)
        pseudo_algo(graph, args.semisupervised_set_path, args.experiment_name / "iter3/data", batch_size=5)
        make_iter1_data(args.dataset_path, args.experiment_name / "iter3/data", graph_cfg.node_names)
        # exit()

        # prepare data
        reader_it3 = NGCNpzReader(args.experiment_name / "iter3/data", nodes=nodes, out_nodes=graph_cfg.output_nodes)

        # train
        graph.vote_fn = vote_fn_median # median for ensemble edges.
        trainer = NGCAsyncTrainer(graph, train_cfg=train_cfg, train_reader=reader_it3,
                                  validation_reader=validation_reader, iter_dir=experiment_dir / "iter3/models",
                                  dataloader_params=loader_params, trainer_params=trainer_params,
                                  seed=cfg.seed, strategy=strategy, multi_train_count=args.multi_train_count)
        trainer()

if __name__ == "__main__":
    main()
