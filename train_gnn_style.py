#!/usr/bin/env python3
"""
GNN-style training for NGC. Work in progress. Mean ensemble only.
"""

from functools import partial
from pathlib import Path
from typing import Dict
from datetime import datetime
from argparse import ArgumentParser
from omegaconf import OmegaConf
import torch as tr
import numpy as np

from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotCallback
from ngclib.logger import logger
from ngclib.readers import NGCNpzReader
from ngclib.ngcdir import NGCDir
from ngclib.trainer import TrainableNGC
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.models import NGC
from ngclib.semisupervised import make_iter1_data
from media_processing_lib.image import image_resize, image_write
from media_processing_lib.collage_maker import collage_fn
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from torch.utils.data import DataLoader

from aggregation import aggregation_fn_from_cfg
from models import get_model_type

VOTE_FN = "MEAN"
accelerator = "gpu" if tr.cuda.is_available() else "cpu"

def nodes_plot_fn(x: Dict[str, tr.Tensor], y: tr.Tensor, gt: Dict[str, tr.Tensor], out_dir: Path, model: LME):
    """Exports x, y and gt for each item in a batch, during training"""
    base_graph: NGC = model.base_model.base_graph
    mb = len(x[base_graph.input_nodes[0]])
    in_nodes = base_graph.input_nodes
    out_nodes = base_graph.output_nodes
    assert len(in_nodes) > 0 and len(out_nodes) > 0

    x_in = [y[node].cpu().numpy() for node in in_nodes]
    y_out = [y[node].cpu().numpy() for node in out_nodes]
    gt_out = [gt[node].cpu().numpy() for node in out_nodes]

    for i in range(mb):
        _x_in = [node.plot_fn(x[i]) for node, x in zip(in_nodes, x_in)]
        _y_out = [node.plot_fn(x[i]) for node, x in zip(out_nodes, y_out)]
        _gt_out = [node.plot_fn(x[i]) for node, x in zip(out_nodes, gt_out)]

        row1 = collage_fn(_x_in, rows_cols=(1, len(_x_in)))
        row2 = collage_fn([*_y_out, *_gt_out], rows_cols=(2, len(_y_out)))
        if row1.shape[1] > row2.shape[1]:
            row1 = image_resize(row1, width=row2.shape[1], height=None)
        else:
            row2 = image_resize(row2, width=row1.shape[1], height=None)
        collage = np.concatenate([row1, row2], axis=0)
        image_write(collage, f"{out_dir}/{i}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the train & graph config path")
    parser.add_argument("--dataset_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--validation_set_path", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--semisupervised_set_path", type=lambda p: Path(p).absolute() if p is not None else None)
    parser.add_argument("--nodes_path", type=Path, help="Path to the nodes implementation used to create the graph")
    parser.add_argument("--parallelize", action="store_true")
    args = parser.parse_args()
    if args.nodes_path is None:
        args.nodes_path = Path.cwd() / "nodes"
        logger.warning(f"--nodes_path not provided. Defaulting to '{args.nodes_path}'")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    graph_cfg = GraphCfg(OmegaConf.to_container(cfg.graph))
    if "experiment_name" not in cfg:
        cfg.experiment_name = args.config_path.stem
        logger.warning(f"Experiment name not provided. Defaulting to '{cfg.experiment_name}'")

    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, nodes_module_path=args.nodes_path).nodes

    validation_reader = NGCNpzReader(path=args.validation_set_path, nodes=nodes, out_nodes=graph_cfg.output_nodes)

    pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))
    experiment_dir = Path(pl_logger.log_dir).absolute()
    logger.info(f"Experiment dir: '{experiment_dir}'")
    make_iter1_data(args.dataset_path, experiment_dir / "iter1/data", graph_cfg.node_names)
    reader_it1 = NGCNpzReader(path=experiment_dir / "iter1/data", nodes=nodes, out_nodes=graph_cfg.output_nodes)

    ngc_edge_fn = partial(get_model_type, str_model_type=cfg.model.type, model_args=cfg.model.parameters)
    vote_fn = aggregation_fn_from_cfg(cfg)
    graph = graph_cfg.build_model(nodes, ngc_edge_fn, vote_fn).to("cpu")
    graph.to_graphviz().render(experiment_dir / "graph", format="png", cleanup=True)

    loader_params = OmegaConf.to_container(cfg.data.loader_params)
    train_cfg = OmegaConf.to_container(cfg.train)
    trainer_params = {**train_cfg["trainer_params"], "accelerator": accelerator,
                      "callbacks": [PlotCallback(nodes_plot_fn)]}
    train_loader = DataLoader(reader_it1, **loader_params)
    val_loader = DataLoader(validation_reader, **{**loader_params, "shuffle": False})
    lme_graph = LME(TrainableNGC(graph, train_cfg))
    lme_graph.save_hyperparameters({"aggregation": OmegaConf.to_container(cfg.graph.aggregation)})
    print(lme_graph.summary)

    Trainer(logger=pl_logger, **trainer_params).fit(lme_graph, train_loader, val_loader)

if __name__ == "__main__":
    main()
